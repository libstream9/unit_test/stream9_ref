#include <stream9/ref.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace st9 { using namespace stream9; }

BOOST_AUTO_TEST_SUITE(ref_)

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        using T1 = st9::ref<int>;
        //using T1 = std::reference_wrapper<int>;
        static_assert(!std::is_constructible_v<T1, int>);
        static_assert(std::is_constructible_v<T1, int&>);
        static_assert(!std::is_constructible_v<T1, int&&>);
        static_assert(!std::is_constructible_v<T1, int const>);
        static_assert(!std::is_constructible_v<T1, int const&>);
        static_assert(!std::is_constructible_v<T1, int const&&>);

        using T2 = st9::ref<int const>;
        //using T2 = std::reference_wrapper<int const>;
        static_assert(!std::is_constructible_v<T2, int>);
        static_assert(std::is_constructible_v<T2, int&>);
        static_assert(!std::is_constructible_v<T2, int&&>);
        static_assert(!std::is_constructible_v<T2, int const>);
        static_assert(std::is_constructible_v<T2, int const&>);
        static_assert(!std::is_constructible_v<T2, int const&&>);
    }

    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        struct foo {};
        struct bar : foo {};

        using T1 = st9::ref<foo>;

        static_assert(!std::is_constructible_v<T1, bar>);
        static_assert(std::is_constructible_v<T1, bar&>);
        static_assert(!std::is_constructible_v<T1, bar&&>);
        static_assert(!std::is_constructible_v<T1, bar const>);
        static_assert(!std::is_constructible_v<T1, bar const&>);
        static_assert(!std::is_constructible_v<T1, bar const&&>);

        using T2 = st9::ref<foo const>;

        static_assert(!std::is_constructible_v<T2, bar>);
        static_assert(std::is_constructible_v<T2, bar&>);
        static_assert(!std::is_constructible_v<T2, bar&&>);
        static_assert(!std::is_constructible_v<T2, bar const>);
        static_assert(std::is_constructible_v<T2, bar const&>);
        static_assert(!std::is_constructible_v<T2, bar const&&>);
    }

    BOOST_AUTO_TEST_CASE(int_)
    {
        int a = 100;

        st9::ref r { a };

        BOOST_TEST(*r == 100);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s = "foo";

        st9::ref r = s;

        BOOST_TEST(*r == "foo");
    }

    BOOST_AUTO_TEST_CASE(polymorphic_type_)
    {
        struct A {};
        struct B : A {} b;

        st9::ref<A> r1 = b;

        BOOST_CHECK(r1 == b);
    }

BOOST_AUTO_TEST_SUITE_END() // ref_

} // namespace testing
